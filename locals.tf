locals {
  tags = {
    Infrastructure = "terraformed"
    CC             = "aubaypt"
    Project        = "webinar-aks"
    Owner          = "nucleo-devops"
  }
}