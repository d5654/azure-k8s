
resource "azurerm_resource_group" "wbr-aks-rg" {
  name     = "wbr-aks-rg"
  location = var.location

  tags = local.tags
}

module "network" {
  source = "Azure/network/azurerm"

  resource_group_name = azurerm_resource_group.wbr-aks-rg.name
  vnet_name           = var.vnet-name
  address_space       = "10.0.0.0/16"
  subnet_prefixes     = ["10.0.0.0/22"]
  subnet_names        = var.subnet-name

  tags       = local.tags
  depends_on = [azurerm_resource_group.wbr-aks-rg]
}

module "aks" {
  source = "Azure/aks/azurerm"

  cluster_name                     = var.cluster-name
  resource_group_name              = azurerm_resource_group.wbr-aks-rg.name
  client_id                        = var.client-id
  client_secret                    = var.client-secret
  kubernetes_version               = var.k8s-version
  orchestrator_version             = var.k8s-version
  prefix                           = "prefix"
  vnet_subnet_id                   = module.network.vnet_subnets[0]
  agents_size                      = var.agents-size
  os_disk_size_gb                  = 50
  sku_tier                         = "Paid" # defaults to Free
  enable_role_based_access_control = false
  #rbac_aad_admin_group_object_ids  = [data.azuread_group.aks_cluster_admins.id] # If enable_aad_admin_group_object_ids set true do you need uncomment this
  #rbac_aad_managed                 = true # If enable_aad_admin_group_object_ids set true do you need uncomment this
  private_cluster_enabled         = false # default value
  enable_log_analytics_workspace  = false
  enable_http_application_routing = true
  enable_azure_policy             = true
  enable_auto_scaling             = true
  enable_host_encryption          = false
  agents_min_count                = 1
  agents_max_count                = 2
  agents_count                    = null # Please set `agents_count` `null` while `enable_auto_scaling` is `true` to avoid possible `agents_count` changes.
  agents_max_pods                 = 100
  agents_pool_name                = var.agent-pool-name
  agents_availability_zones       = ["1", "2"]
  agents_type                     = "VirtualMachineScaleSets"

  public_ssh_key = var.ssh-pub-key

  agents_labels = {
    "nodepool" : "defaultnodepool"
  }

  agents_tags = {
    "Agent" : "defaultnodepoolagent"
  }

  enable_ingress_application_gateway      = true
  ingress_application_gateway_name        = "aks-agw"
  ingress_application_gateway_subnet_cidr = "10.0.10.0/24"

  network_plugin                 = "azure"
  network_policy                 = "azure"
  net_profile_dns_service_ip     = cidrhost(var.service-cidr, 10)
  net_profile_docker_bridge_cidr = "172.17.0.1/16"
  net_profile_service_cidr       = var.service-cidr

  tags       = local.tags
  depends_on = [module.network]
}