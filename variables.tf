variable "location" {
  description = "Azure region to use"
  type        = string
}

variable "cluster-name" {
  description = "AKS name"
  type        = string
}

variable "vnet-name" {
  description = "Virtual Network name"
  type        = string
}

variable "subnet-name" {
  description = "Subnet name"
  type        = list(string)
}

variable "service-cidr" {
  description = "CIDR used by kubernetes services (kubectl get svc)"
  type        = string
}

variable "k8s-version" {
  description = "Version of kubernetes to deploy"
  type        = string
}

variable "agent-pool-name" {
  description = "Agent pool name"
  type        = string
}

variable "agents-size" {
  description = "The default virtual machine size for the Kubernetes agents"
  type        = string
}

variable "client-id" {
  description = ""
  type        = string
}

variable "client-secret" {
  description = ""
  type        = string
}

variable "ssh-pub-key" {
  description = "Your SSH Public Key"
  type        = string
}