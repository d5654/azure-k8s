location        = "westus2"
vnet-name       = "wbr-aks-vnet"
subnet-name     = ["wbr-aks-sbnt1"]
cluster-name    = "wbr-aks-nucleo-devops"
agent-pool-name = "wbraksagtp1"
agents-size     = "Standard_D2_v5"
k8s-version     = "1.22.4"
service-cidr    = "10.100.0.0/16"
ssh-pub-key     = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDMKylJTk6TuNMEANfhuQAeFdi8iZZrwi89zZGKgIaRG6tBI0DNLWcTR/iZ6ccjv264XjAU8xyPDomPbaAaHRtLiJtzoDT8yT4QlNTpljNbXyqOkIjLxAGOeEwb/GrD5200PWPGPEo94moHNpeFT5nFkCT3Y5JlW7dsSAtxT/FcPHNlSJawE0tnZs1p70Z67K+xhxuPj3PSd76MgJLxgSFOmaQuiuA+s06ut0e9e0Ejc+c8DdAeHwoo1zzaKNTkAxr8ZwLbfNbWw+LStrQE6x+/5suaCHJpaYKQo8+dC20AB3dprq/X3GBL2vpFpWBNwZpR3lDQTUnKfhnIeS5fA7OmmR7HRMdc9J4nNsmSHoIjlnwlsRqyiz6wecIasF08I4rYkHTdDzJeApeVekaANtJ6iiCsvNiqSO/dR9j9IIaTFkKAN+zq70iJMfp4+8U2UdNA86jztpwhR4TmnScQ6G3iG3CsEtmO8zJsoesUK3s0iNUjK563+TGTr7Xso++qXm+UJD614+2oM6YW6ZNpIan/cbKSarRA8HyO3gWVUoJpN8XQX62+ZfMMUlVW4VP6ObwJ/P++SFpVYqh6922Op4AVJulB8qZf11+skEZbO6TiUYfMio8sNN55zrET5PRO0hNPbLEC2h++9MzGKbJETtMN1ErBPAR922ZBSMvkGGNeHQ=="